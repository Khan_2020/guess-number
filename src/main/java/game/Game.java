package game;

import answer.Answer;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Game {

    private Answer answer;
    private Map<String, String> guessResult;

    public Game() {
        Path filePath = Paths.get("answer.txt");
        this.answer = new Answer(filePath);
        this.guessResult = new LinkedHashMap<>();
    }

    public String guess(List<Integer> numbers) {
        String result = "";
        if (numbers.equals(answer.getAnswer())) {
            result = "4A0B";
        } else {
            int countA = getASize(numbers);
            int countB = getBSize(numbers);
            result = countA + "A" + countB + "B";
        }
        String key = numbers.stream().map(String::valueOf).collect(Collectors.joining(""));
        this.guessResult.put(key, result);
        return result;
    }

    public boolean isOver() {
        if (guessResult.size() == 6) return true;
        for (Map.Entry<String, String> entry : guessResult.entrySet()) {
            if (entry.getValue() == "4A0B") return true;
        }
        return false;
    }

    public String getResult() {
        boolean flag = false;
        StringBuilder resultMessage = new StringBuilder();
        for (Map.Entry<String, String> entry : guessResult.entrySet()) {
            resultMessage.append(entry.getKey() + " ");
            resultMessage.append(entry.getValue() + "\n");
            if (entry.getValue() == "4A0B") flag = true;
        }
        if (flag)
            resultMessage.append("Congratulations, you win!");
        else
            resultMessage.append("Unfortunately, you have no chance, the answer is 1234!");
        return resultMessage.toString();
    }

    private int getBSize(List<Integer> numbers) {
        int count = 0;
        HashSet<Integer> nums = new HashSet<>();
        for (Integer i : numbers) nums.add(i);
        for (int i = 0; i < 4; i++) {
            if (nums.contains(answer.getAnswer().get(i)))
                count++;
        }
        return count - getASize(numbers);
    }

    private int getASize(List<Integer> numbers) {
        int count = 0;
        for (int i = 0; i < 4; i++) {
            if (numbers.get(i) == answer.getAnswer().get(i))
                count++;
        }
        return count;
    }
}
