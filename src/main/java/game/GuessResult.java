package game;

import java.util.Map;

public class GuessResult {
    private Map<String, String> guessRecord;
    private static final int CHANCE_LIMIT = 6;

    public GuessResult(Map<String, String> guessRecord) {
        this.guessRecord = guessRecord;
    }

    public String getResult() {
        StringBuilder result = new StringBuilder();
        for (String key : guessRecord.keySet()) {
            result.append(key + " ").append(guessRecord.get(key) + "\n");
        }
        result.delete(result.length() - 1, result.length());
        return result.toString();
    }

    public GameResult getGameResult() {
        for (Map.Entry<String, String> entry : guessRecord.entrySet()) {
            if(entry.getValue()=="4A0B") return GameResult.WIN;
        }
        if(guessRecord.size()<6) return GameResult.NORMAL;
        return GameResult.LOST;
    }
}
