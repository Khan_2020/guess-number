package answer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;


public class Answer {

    private static final int ANSWER_LENGTH = 4;
    private static final int ANSWER_NUMBER_LIMIT = 10;
    private List<Integer> answer = null;

    public Answer(Path filePath) {
        try {
            List<Integer> numbers = getAnswerFromFile(filePath);
            validAnswer(numbers);
            this.setAnswer(numbers);
        } catch (Exception e) {
            this.setAnswer(generateRandomAnswer());
        }
    }

    public List<Integer> getAnswer() {
        return this.answer;
    }

    public void setAnswer(List<Integer> answer) {
        this.answer = answer;
    }

    @Override
    public String toString() {
        return answer.stream().map(String::valueOf).collect(Collectors.joining(""));
    }

    private List<Integer> getAnswerFromFile(Path filePath) throws IOException {
        Objects.requireNonNull(filePath);
        ClassLoader classLoader = getClass().getClassLoader();
        InputStream is = classLoader.getResourceAsStream(filePath.toString());
        Objects.requireNonNull(is);
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        String number = reader.readLine();
        return number.chars().mapToObj(o -> Character.getNumericValue(o)).collect(Collectors.toList());
    }

    private List<Integer> generateRandomAnswer() {
        List<Integer> randomAnswer = new ArrayList<>();
        for (int i = 0; i < ANSWER_LENGTH; i++)
            randomAnswer.add(new Random().nextInt(ANSWER_NUMBER_LIMIT + 1));
        return randomAnswer;
    }

    static void validAnswer(List<Integer> answer) throws InvalidAnswerException {
        if (answer.size() != 4) throw new InvalidAnswerException("Wrong input");
        HashSet<Integer> answerCheck = new HashSet<Integer>();
        for (Integer i : answer) answerCheck.add(i);
        if (answerCheck.size() != 4) throw new InvalidAnswerException("Wrong input");
    }
}
